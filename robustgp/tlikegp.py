import GPy
import numpy as np
from scipy.stats import chi2
from collections import namedtuple
import time
__all__ = ['TLIKEGP']


def TLIKEGP(X, Y, X_true, Y_true, type, optimize_kwargs={}, **gp_kwargs):

    start_time = time.time()

    # check parameters
    if X.ndim == 1:
        X = np.atleast_2d(X).T
    if X_true.ndim == 1:
        X_true = np.atleast_2d(X).T
    if Y.ndim == 1:
        Y = np.atleast_2d(Y).T
    if len(X) != len(Y):
        raise ValueError("X should have the same length as Y")

    n, p = Y.shape
    if p != 1:
        raise ValueError("Y is expected in shape (n, 1).")

    gp_kwargs.setdefault('likelihood', GPy.likelihoods.StudentT())
    gp_kwargs.setdefault('kernel', GPy.kern.RBF(X.shape[1]))
    gp_kwargs.setdefault('name', 'TLIKEGP regression')

    # use copies so that input likelihood and kernel will not be changed
    likelihood_init = gp_kwargs['likelihood'].copy()
    kernel_init = gp_kwargs['kernel'].copy()

    if type == 'regression':
        gp = GPy.models.GPVariationalGaussianApproximation(X, Y, kernel=kernel_init,
                                                           likelihood=likelihood_init)
    else:
        gp = GPy.core.GP(X, Y, **gp_kwargs)
    gp.optimize(**optimize_kwargs)
    Y_avg, Y_var = gp.predict(X, include_likelihood=True)

    duration_in_seconds = time.time() - start_time

    Y_true_avg, _ = gp.predict(X_true, include_likelihood=True)
    diff = np.subtract(Y_true, Y_true_avg)
    diffsq = np.power(diff, 2.0)
    mse_new = np.mean(diffsq)
    #mse = sum((Y_true - Y_avg) ** 2) / len(Y_true)

    return TLIKEGPResult(gp, Y_avg, Y_var,duration_in_seconds,mse_new)

TLIKEGPResult = namedtuple('TLIKEGPResult', ('gp', 'Y_avg', 'Y_var','duration_in_seconds','mse'))
