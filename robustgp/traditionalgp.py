import GPy
import numpy as np
import time
from collections import namedtuple

__all__ = ['TRADGP']


def TRADGP(X, Y, X_true, Y_true,type,optimize_kwargs={}, **gp_kwargs):

    start_time = time.time()

    # check parameters
    if X.ndim == 1:
        X = np.atleast_2d(X).T
    if X_true.ndim == 1:
        X_true = np.atleast_2d(X).T
    if Y.ndim == 1:
        Y = np.atleast_2d(Y).T
    if len(X) != len(Y):
        raise ValueError("X should have the same length as Y")

    n, p = Y.shape
    if p != 1:
        raise ValueError("Y is expected in shape (n, 1).")

    gp_kwargs.setdefault('likelihood', GPy.likelihoods.Gaussian(variance=0.1))
    gp_kwargs.setdefault('kernel', GPy.kern.RBF(X.shape[1]))
    gp_kwargs.setdefault('name', 'ITGP regression')

    # use copies so that input likelihood and kernel will not be changed
    likelihood_init = gp_kwargs['likelihood'].copy()
    kernel_init = gp_kwargs['kernel'].copy()

    if type == 'regression':
        gp = GPy.models.GPRegression(X, Y, kernel_init, noise_var=0.1)
    else:
        gp = GPy.core.GP(X, Y, **gp_kwargs)
    gp.optimize(**optimize_kwargs)
    Y_avg, Y_var = gp.predict(X, include_likelihood=True)

    duration_in_seconds = time.time() - start_time

    Y_true_avg, _ = gp.predict(X_true, include_likelihood=True)
    diff = np.subtract(Y_true, Y_true_avg)
    diffsq = np.power(diff, 2.0)
    mse_new = np.mean(diffsq)

    return TRADGPResult(gp, Y_avg, Y_var,duration_in_seconds,mse_new)

TRADGPResult = namedtuple('TRADGPResult', ('gp', 'Y_avg', 'Y_var','duration_in_seconds','mse'))
