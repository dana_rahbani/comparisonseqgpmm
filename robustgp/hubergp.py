
import GPy
import numpy as np
from scipy.stats import chi2
from collections import namedtuple
import time

__all__ = ['HUBGP']


def HUBGP(X, Y, X_true, Y_true, ix_sub, type,niter_max=20, convergence_threshold = 0.001, huber_threshold = 0.3, optimize_kwargs={}, **gp_kwargs):
    """based on
    Robust Gaussian Process Regression Based on Iterative Trimming.

    Parameters
    ----------
    X: array shape (n, d)
    Y: array shape (n, 1)
    ix_sub: array with initial inlier indices
    CI: confidence interval for computing threshold, between 0 and 100

    Returns
    -------
    ITGPResult: named tuple object
        gp:
            GPy.core.GP object.
        ix_sub:
            Boolean index for trimming sample.
        niter:
            Total iterations performed, <= 1 + nsh + ncc + nrw.
        Y_avg, Y_var:
            Expectation and variance of input data points. None if return_predict=False.
        threshold:
            computed threshold
    """

    start_time = time.time()

    # check parameters
    if X.ndim == 1:
        X = np.atleast_2d(X).T
    if X_true.ndim == 1:
        X_true = np.atleast_2d(X).T
    if Y.ndim == 1:
        Y = np.atleast_2d(Y).T
    if len(X) != len(Y):
        raise ValueError("X should have the same length as Y")

    n, p = Y.shape
    if p != 1:
        raise ValueError("Y is expected in shape (n, 1).")

    gp_kwargs.setdefault('likelihood', GPy.likelihoods.Gaussian(variance=0.1))
    gp_kwargs.setdefault('kernel', GPy.kern.RBF(X.shape[1]))
    gp_kwargs.setdefault('name', 'ITGP regression')

    # use copies so that input likelihood and kernel will not be changed
    likelihood_init = gp_kwargs['likelihood'].copy()
    kernel_init = gp_kwargs['kernel'].copy()

    niter = 0
    d_avg = 10.0

    while d_avg>convergence_threshold and niter<niter_max:
        niter = niter+1
        if type == 'regression':
            gp = GPy.models.GPRegression(X[ix_sub], Y[ix_sub], kernel_init, noise_var=0.1)
        else:
            gp = GPy.core.GP(X[ix_sub], Y[ix_sub], **gp_kwargs)
        gp.optimize(**optimize_kwargs)
        # make prediction
        Y_avg, Y_var = gp.predict(X, include_likelihood=True)
        ds = (np.abs(Y - Y_avg)).ravel()#((Y - Y_avg)**2 / Y_var).ravel()
        for index,d in enumerate(ds):
            if d<huber_threshold:
                ds[index]=d*d/2.0
            else:
                ds[index]=huber_threshold*(d-huber_threshold*0.5)
        ix_sub = [index for index, value in enumerate(ds) if value<huber_threshold]
        #todo remove classification step?
        d_avg = np.mean(ds)
        Y_true_avg, _ = gp.predict(X_true, include_likelihood=True)
        diff = np.subtract(Y_true, Y_true_avg)
        diffsq = np.power(diff, 2.0)
        mse_new = np.mean(diffsq)
        #mse_new = sum((Y_true - Y_avg)**2)/len(Y_true)

    duration_in_seconds = time.time() - start_time


    return HUBGPResult(gp, ix_sub, niter, Y_avg, Y_var,duration_in_seconds,mse_new)

HUBGPResult = namedtuple('HUBGPResult', ('gp', 'ix_sub', 'niter', 'Y_avg', 'Y_var','duration_in_seconds','mse'))
