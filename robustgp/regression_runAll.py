import numpy as np
from matplotlib import pyplot as plt
from sequentialgp import SEQGP
from traditionalgp import TRADGP
from tlikegp import TLIKEGP
from bowithoutliergp import BOWOGP
from hubergp import HUBGP
from robustgp import ITGP
import csv
plt.rcParams.update({'font.size': 22})

__all__ = ['RUN']

def neal_func(x):
    return 0.3 + 0.4 * x + 0.5 * np.sin(2.7 * x) + 1.1 / (1 + x**2)

def generate_data(outlier_type_number, n):
    means = 0
    sds = 0.1
    x_ob = np.random.rand(n) * 6 - 3
    y_ob = neal_func(x_ob) + np.random.normal(means, sds, n)
    y_ob_true = neal_func(x_ob)
    x_ob_true = x_ob
    if outlier_type_number == 0:
        case = "case: zero outliers"
        mean_outlier = 0
        sd_outlier = 0
        noise = 1.0
        noise_outlier = 1.0 - noise
        n_outlier = int(noise_outlier * n)
        outlier_indices = np.arange(0, n_outlier)
        y_ob[outlier_indices] = neal_func(x_ob[outlier_indices]) + np.random.normal(mean_outlier, sd_outlier, n_outlier)
    elif outlier_type_number ==1:
        case = "case: rare outliers"
        mean_outlier = 0
        sd_outlier = 1
        noise = 0.95
        noise_outlier = 1.0 - noise
        n_outlier = int(noise_outlier*n)
        outlier_indices = np.arange(0,n_outlier)
        y_ob[outlier_indices] = neal_func(x_ob[outlier_indices]) + np.random.normal(mean_outlier, sd_outlier,n_outlier)
    elif outlier_type_number==2:
        case = "case: fiducial outliers"
        mean_outlier = 0
        sd_outlier = 1
        noise = 0.85
        noise_outlier = 1.0 - noise
        n_outlier = int(noise_outlier*n)
        outlier_indices = np.arange(0,n_outlier)
        y_ob[outlier_indices] = neal_func(x_ob[outlier_indices]) + np.random.normal(mean_outlier, sd_outlier,n_outlier)
    elif outlier_type_number==3:
        case = "case: abundant outliers"
        mean_outlier = 0
        sd_outlier = 1
        noise = 0.55
        noise_outlier = 1.0 - noise
        n_outlier = int(noise_outlier*n)
        outlier_indices = np.arange(0,n_outlier)
        y_ob[outlier_indices] = neal_func(x_ob[outlier_indices]) + np.random.normal(mean_outlier, sd_outlier,n_outlier)
    elif outlier_type_number==4:
        case = "case: skewed outliers"
        mean_outlier = 2
        sd_outlier = 1
        noise = 0.85
        noise_outlier = 1.0 - noise
        n_outlier = int(noise_outlier*n)
        outlier_indices = np.arange(0,n_outlier)
        y_ob[outlier_indices] = neal_func(x_ob[outlier_indices]) + np.random.normal(mean_outlier, sd_outlier,n_outlier)
    elif outlier_type_number==5:
        case = "case: extreme outliers"
        mean_outlier = 0
        sd_outlier = 5
        noise = 0.85
        noise_outlier = 1.0 - noise
        n_outlier = int(noise_outlier*n)
        outlier_indices = np.arange(0,n_outlier)
        y_ob[outlier_indices] = neal_func(x_ob[outlier_indices]) + np.random.normal(mean_outlier, sd_outlier,n_outlier)
    elif outlier_type_number == 6:
        case = "case: uniform outliers"
        noise = 0.7
        noise_outlier = 1.0 - noise
        n_outlier = int(noise_outlier*n)
        outlier_indices = np.arange(0,n_outlier)
        y_ob[outlier_indices] = neal_func(x_ob[outlier_indices]) + np.random.uniform(-3.0,3.0,n_outlier)
    elif outlier_type_number == 7:
        case = "case: t3 distribution"
        y_ob = neal_func(x_ob) + np.random.standard_t(3,n)
        outlier_indices = np.arange(0, 0)
    elif outlier_type_number == 8:
        case = "case: t1 distribution"
        y_ob = neal_func(x_ob) + np.random.standard_cauchy(n)
        outlier_indices = np.arange(0, 0)
    elif outlier_type_number == 9:
        n_outlier = 50
        case = "case: smooth outliers - bump"
        outlier_indices = x_ob.argsort()[:n_outlier]
        y_ob[outlier_indices] = neal_func(x_ob[outlier_indices]) - neal_func(x_ob[outlier_indices])
    elif outlier_type_number == 10:
        n_outlier = 50
        case = "case: smooth outliers - hole"
        outlier_indices = x_ob.argsort()[:n_outlier]
        inlier_indices = [i for i in np.arange(0,n) if i not in outlier_indices]
        x_ob=x_ob[inlier_indices]
        y_ob=y_ob[inlier_indices]
    else:
        sys.exit('invalid outlier type')
    return (x_ob,y_ob, x_ob_true, y_ob_true, outlier_indices,case)

def RUN(case_number, total_number_observations = 500,
        function=neal_func, percentage_landmarks=3, total_number_iterations=0,
        type = 'optimization'):

    number_landmarks = int(percentage_landmarks*0.01*total_number_observations)
    print("**********case number**********")
    print(case_number)

    #set grid
    grid_steps = 51
    x = np.linspace(-3, 3, grid_steps)
    r_trad = []
    r_seq = []
    r_hub = []
    r_it = []
    t_trad = []
    t_seq = []
    t_hub = []
    t_it = []
    mse_trad = []
    mse_seq = []
    mse_hub = []
    mse_it = []

    # get True prediction
    y_tr = function(x)

    for iteration_number in np.arange(0, total_number_iterations):

        print("--iteration_number--")
        print(iteration_number)

        np.random.seed(iteration_number)

        x_ob, y_ob, x_ob_true,y_ob_true, outlier_indices, case = generate_data(case_number, total_number_observations)

        diffs = np.absolute(function(x_ob) - y_ob)
        init_inlier_indices = diffs.argsort()[:number_landmarks]

        print("TRADGP")
        # traditional GP
        m_tradgp = TRADGP(x_ob, y_ob, x_ob_true, y_ob_true, optimize_kwargs=dict(optimizer='lbfgsb'), type = type)
        gp_tradgp, time_tradgp, mse_tradgp = m_tradgp.gp, m_tradgp.duration_in_seconds, m_tradgp.mse
        y_avg_tradgp, y_var_tradgp = gp_tradgp.predict(x.reshape(-1, 1))
        y_avg_tradgp, y_var_tradgp = y_avg_tradgp.ravel(), y_var_tradgp.ravel()

        print("SEQGP")
        # sequential GP
        m_seqgp = SEQGP(x_ob, y_ob, x_ob_true, y_ob_true, init_inlier_indices, CI=97.5, optimize_kwargs=dict(optimizer='lbfgsb'), type = type)
        gp_seqgp, time_seqgp, mse_seqgp = m_seqgp.gp, m_seqgp.duration_in_seconds, m_seqgp.mse
        y_avg_seqgp, y_var_seqgp = gp_seqgp.predict(x.reshape(-1, 1))
        y_avg_seqgp, y_var_seqgp = y_avg_seqgp.ravel(), y_var_seqgp.ravel()

        print("HUBGP")
        # huber loss GP
        reg_hubgp = HUBGP(x_ob, y_ob, x_ob_true, y_ob_true, init_inlier_indices, optimize_kwargs=dict(optimizer='lbfgsb'), type = type)
        gp_hubgp, time_hubgp, mse_hubgp = reg_hubgp.gp, reg_hubgp.duration_in_seconds, reg_hubgp.mse
        y_avg_hubgp, y_var_hubgp = gp_hubgp.predict(x.reshape(-1, 1))
        y_avg_hubgp, y_var_hubgp = y_avg_hubgp.ravel(), y_var_hubgp.ravel()

        print("ITGP")
        # https://www.sciencedirect.com/science/article/pii/S2213133721000378
        # ITGP
        res = ITGP(x_ob, y_ob, x_ob_true, y_ob_true,
                   alpha1=0.5, alpha2=0.975, nsh=2, ncc=2, nrw=1,
                   optimize_kwargs=dict(optimizer='lbfgsb'), type = type
                   )
        gp, consistency, time_itgp, mse_itgp = res.gp, res.consistency, res.duration_in_seconds, res.mse
        y_avg_itgp, y_var_itgp = gp.predict(x.reshape(-1, 1))
        y_avg_itgp, y_var_itgp = y_avg_itgp.ravel(), y_var_itgp.ravel()
        y_var_itgp *= consistency

        #prepare data for plotting
        y_lim_lower = -3
        y_lim_upper = 3
        extreme_outlier_indices_lower = np.zeros(1, dtype=int)
        extreme_outlier_indices_upper = np.zeros(1, dtype=int)
        regular_outlier_indices = np.zeros(1, dtype=int)
        if (len(outlier_indices) != 0) and (int(case_number)!=10):
            print(len(outlier_indices))
            for index,value in enumerate(y_ob):
                if index in outlier_indices:
                    if value<y_lim_lower:
                        extreme_outlier_indices_lower = np.append(extreme_outlier_indices_lower, index)
                    elif value>y_lim_upper:
                        extreme_outlier_indices_upper = np.append(extreme_outlier_indices_upper, index)
                    else:
                        regular_outlier_indices = np.append(regular_outlier_indices, index)
            np.delete(extreme_outlier_indices_lower, 0)
            np.delete(extreme_outlier_indices_upper, 0)
            np.delete(regular_outlier_indices, 0)
        methods = ['TRADGP', 'ITGP', 'BOWOGP', 'TLIKE', 'HUBGP', 'SEQGP']
        colors = ['C1', 'C2', 'C9', 'C4', 'C6', 'C8']

        # plot fits
        plt.figure(figsize=(20, 15))
        plt.scatter(x_ob, y_ob, facecolor='none', edgecolor='C0', zorder=-1)
        if (len(outlier_indices) != 0) and (int(case_number)!=10):
            plt.scatter(x_ob[extreme_outlier_indices_upper], np.ones(len(extreme_outlier_indices_upper)) * y_lim_upper, marker ='^', facecolor='none', edgecolor='C3', zorder=-1,label = 'extreme upper outlier points')
            plt.scatter(x_ob[extreme_outlier_indices_lower], np.ones(len(extreme_outlier_indices_lower)) * y_lim_lower, marker ='v', facecolor='none', edgecolor='C3', zorder=-1, label = 'extreme lower outlier points')
            plt.scatter(x_ob[regular_outlier_indices], y_ob[regular_outlier_indices], facecolor='none', edgecolor='C3', zorder=-1, label = 'extreme outlier points')
        plt.plot(x, y_tr, color='k', lw=3, ls='--', label='True')
        plt.plot(x, y_avg_tradgp, color=colors[0], lw=2, label=methods[0])
        plt.plot(x, y_avg_itgp, color=colors[1], lw=2, label=methods[1])
        plt.plot(x, y_avg_hubgp, color=colors[4], lw=2, label=methods[4])
        plt.plot(x, y_avg_seqgp, color=colors[5], lw=2, label=methods[5])
        plt.scatter(x_ob[init_inlier_indices],y_ob[init_inlier_indices], color = 'k', label = 'given landmarks')
        plt.legend()
        plt.ylim(y_lim_lower,y_lim_upper)
        plt.title(case)
        plt.savefig(type+"/lms" + str(percentage_landmarks) + "case"+str(case_number) + "-iteration"+str(iteration_number) + ".pdf")

        #plot durations in seconds
        plt.figure(figsize=(20, 15))
        xaxis = ['tradgp', 'itgp', 'hubgp', 'seqgp']
        yaxis = [time_tradgp, time_itgp, time_hubgp, time_seqgp]
        t_trad = np.append(t_trad,time_tradgp)
        t_it = np.append(t_it, time_itgp)
        t_hub = np.append(t_hub, time_hubgp)
        t_seq = np.append(t_seq, time_seqgp)
        categories = np.array([0,1,4,5])
        plt.scatter(xaxis,yaxis, c = np.array(colors)[categories])
        plt.title("Time per method")
        plt.xlabel('method')
        plt.ylabel('duration in seconds')
        plt.yticks(np.arange(0,max(yaxis),step = 50))
        plt.savefig(type+"/lms" + str(percentage_landmarks) + "case"+ str(case_number)+ "-iteration"+str(iteration_number) +"-time.pdf")

        #plot mean squared errors
        mse_trad = np.append(mse_trad, mse_tradgp)
        mse_it = np.append(mse_it, mse_itgp)
        mse_hub = np.append(mse_hub, mse_hubgp)
        mse_seq = np.append(mse_seq, mse_seqgp)
        yaxis = [mse_tradgp, mse_itgp, mse_hubgp, mse_seqgp]
        plt.scatter(xaxis, yaxis, c=np.array(colors)[categories])
        plt.title("MSE per method")
        plt.xlabel('method')
        plt.ylabel('mse')
        plt.yticks(np.arange(0, max(yaxis), step=50))
        plt.savefig(type+"/lms" + str(percentage_landmarks) + "case" + str(case_number) + "-iteration" + str(
            iteration_number) + "-mse.pdf")

        # save fits
        r_trad = np.append(r_trad,y_avg_tradgp)
        r_seq = np.append(r_seq,y_avg_seqgp)
        r_it = np.append(r_it,y_avg_itgp)
        r_hub = np.append(r_hub,y_avg_hubgp)

        plt.close('all')

    #get average and std over iterations
    r_trad = r_trad.reshape(total_number_iterations,grid_steps)
    r_seq = r_seq.reshape(total_number_iterations,grid_steps)
    r_it = r_it.reshape(total_number_iterations, grid_steps)
    r_hub = r_hub.reshape(total_number_iterations, grid_steps)
    r_trad_mean = np.mean(r_trad,axis = 0)
    r_seq_mean = np.mean(r_seq,axis = 0)
    r_it_mean = np.mean(r_it,axis = 0)
    r_hub_mean = np.mean(r_hub,axis = 0)
    r_trad_std = np.std(r_trad,axis = 0)
    r_seq_std = np.std(r_seq,axis = 0)
    r_it_std = np.std(r_it,axis = 0)
    r_hub_std = np.std(r_hub,axis = 0)

    #plot average and std over iterations
    plt.figure(figsize=(20, 15))
    plt.plot(x, y_tr, color='k', lw=3, ls='--', label='True')
    plt.plot(np.linspace(-3, 3, 51), r_trad_mean, color=colors[0], label=methods[0])
    plt.plot(np.linspace(-3, 3, 51), r_it_mean, color=colors[1], label=methods[1])
    plt.plot(np.linspace(-3, 3, 51), r_hub_mean, color=colors[4], label=methods[4])
    plt.plot(np.linspace(-3, 3, 51), r_seq_mean, color=colors[5], label=methods[5])
    plt.fill_between(x, r_seq_mean - r_seq_std, r_seq_mean + r_seq_std, lw=0, color=colors[5], alpha=0.6, zorder=-2)
    plt.fill_between(x, r_hub_mean - r_hub_std, r_hub_mean + r_hub_std, lw=0, color=colors[4], alpha=0.6, zorder=-2)
    plt.fill_between(x, r_it_mean - r_it_std, r_it_mean + r_it_std, lw=0, color=colors[1], alpha=0.6, zorder=-2)
    plt.fill_between(x, r_trad_mean - r_trad_std, r_trad_mean + r_trad_std, lw=0, color=colors[0], alpha=0.6, zorder=-2)
    plt.title(case + "\n average of " + str(total_number_iterations) + " iterations")
    plt.legend()
    plt.savefig(type+"/lms" + str(percentage_landmarks) + "case" + str(case_number) + "-averageFrom" + str(total_number_iterations) + ".pdf")
    plt.figure(figsize=(20, 15))
    plt.boxplot([t_trad,t_it,t_hub,t_seq])
    plt.xlabel(['tradgp','itgp','hubgp','seqgp'])
    plt.savefig(type+"/lms" + str(percentage_landmarks) + "case" + str(case_number)+"-averageFrom"+ str(total_number_iterations) + "-time.pdf")
    plt.figure(figsize=(20, 15))
    plt.boxplot([mse_trad, mse_it, mse_hub, mse_seq])
    plt.xlabel(['tradgp', 'itgp', 'hubgp', 'seqgp'])
    plt.savefig(type+"/lms" + str(percentage_landmarks) + "case" + str(case_number) + "-averageFrom" + str(total_number_iterations) + "-mse.pdf")
    plt.close('all')

    return (t_trad, t_it, t_hub, t_seq,mse_trad, mse_it, mse_hub, mse_seq)


