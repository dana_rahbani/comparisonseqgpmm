import GPy
import numpy as np
import scipy.stats as ss
from scipy.optimize import minimize
from collections import namedtuple
import time


__all__ = ['BOWOGP']

def BOWOGP(X, Y, X_true,Y_true, ix_sub, type, optimize_kwargs={}, **gp_kwargs):

    start_time = time.time()

    # check parameters
    if X.ndim == 1:
        X = np.atleast_2d(X).T
    if X_true.ndim == 1:
        X_true = np.atleast_2d(X).T
    if Y.ndim == 1:
        Y = np.atleast_2d(Y).T
    if len(X) != len(Y):
        raise ValueError("X should have the same length as Y")

    n, p = Y.shape
    if p != 1:
        raise ValueError("Y is expected in shape (n, 1).")

    gp_kwargs.setdefault('likelihood', GPy.likelihoods.StudentT())
    gp_kwargs.setdefault('kernel', GPy.kern.RBF(X.shape[1]))
    gp_kwargs.setdefault('name', 'TLIKEGP regression')

    # use copies so that input likelihood and kernel will not be changed
    likelihood_init = gp_kwargs['likelihood'].copy()
    kernel_init = gp_kwargs['kernel'].copy()

    T = 20#int(len(X)*0.8)
    n_iter = 0
    ix_sub_old = ix_sub
    ix_sub = np.zeros(len(ix_sub_old),dtype=int)
    for index,val in enumerate(ix_sub_old):
        ix_sub[index] = int(val)
    ix_sub_new = np.zeros(1, dtype=int)  # drop first element later
    while n_iter<T:

        n_iter = n_iter+1

        #GP with tLike
        if type == 'regression':
            gp_tlike = GPy.models.GPVariationalGaussianApproximation(X[ix_sub], Y[ix_sub], kernel=kernel_init,
                                                                     likelihood=likelihood_init)
        else:
            gp_tlike = GPy.core.GP(X[ix_sub], Y[ix_sub], **gp_kwargs)
        gp_tlike.optimize(**optimize_kwargs)
        Y_avg_tlike, Y_var_tlike = gp_tlike.predict(X, include_likelihood=True)

        # Filtering
        if len(ix_sub[ix_sub!=0])>len(Y)*0.5:
            lower_thresholds = np.empty(len(Y_avg_tlike))
            upper_thresholds = np.empty(len(Y_avg_tlike))
            for i in range(0, len(Y_avg_tlike)):
                lower_thresholds[i] = Y_avg_tlike[i] - 1.645*np.sqrt(Y_var_tlike[i])#alpha = 0.05, 5percentile
                upper_thresholds[i] = Y_avg_tlike[i] + 1.645*np.sqrt(Y_var_tlike[i])#alpha = 0.05, 95percentile
                #todo find bug, why is var negative from gptlike GPVariationalGaussianApproximation
            for ind,yval in enumerate(Y):
                if (yval<upper_thresholds[ind]) and (yval>lower_thresholds[ind]):
                    ix_sub_new=np.append(ix_sub_new,ind)#ix_sub[ind]
        else:
            for main_ind,main_val in enumerate(Y):
                ix_sub_new = ix_sub
                #ix_sub_new=np.append(ix_sub_new,main_ind)
        if n_iter==1:
            ix_sub_new = np.delete(ix_sub_new,0)#drop first element

        ix_sub_new = np.unique(ix_sub_new)

        #GP with Glike
        gp_kwargs.clear()
        gp_kwargs.setdefault('likelihood', GPy.likelihoods.Gaussian(variance=0.1))
        gp_kwargs.setdefault('kernel', GPy.kern.RBF(X.shape[1]))
        gp_kwargs.setdefault('name', 'BOWOGP regression')

        if type == 'regression':
            gp_glike = GPy.models.GPRegression(X[ix_sub_new], Y[ix_sub_new], kernel_init, noise_var=0.1)
        else:
            gp_glike = GPy.core.GP(X[ix_sub_new], Y[ix_sub_new], **gp_kwargs)
        gp_glike.optimize(**optimize_kwargs)
        Y_avg_glike, Y_var_glike = gp_glike.predict(X, include_likelihood=True)
        Y_true_avg_glike, _ = gp_glike.predict(X_true, include_likelihood=True)
        diff = np.subtract(Y_true, Y_true_avg_glike)
        diffsq = np.power(diff, 2.0)
        mse_new = np.mean(diffsq)
        #mse_new = sum((Y_true - Y_avg_glike)**2)/len(Y_true)

        bounds = np.array([[-3.0, 3.0]])
        new_x = propose_location(expected_improvement, X, Y, gp_glike, bounds) #ix_sub for X and Y?
        diffs = np.absolute(X-new_x)
        closest_index = diffs.argmin()
        ix_sub_new = np.append(ix_sub_new,closest_index)
        ix_sub = ix_sub_new
    duration_in_seconds = time.time() - start_time

    return BOWOGPResult(gp_glike, Y_avg_glike, Y_var_glike,duration_in_seconds,mse_new)

# expected_improvement and propose_location from http://krasserm.github.io/2018/03/21/bayesian-optimization/

def expected_improvement(X, X_sample, Y_sample, gpr, xi=0.01):
    '''
    Computes the EI at points X based on existing samples X_sample
    and Y_sample using a Gaussian process surrogate model.

    Args:
        X: Points at which EI shall be computed (m x d).
        X_sample: Sample locations (n x d).
        Y_sample: Sample values (n x 1).
        gpr: A GaussianProcessRegressor fitted to samples.
        xi: Exploitation-exploration trade-off parameter.

    Returns:
        Expected improvements at points X.
    '''
    mu, sigma = gpr.predict(X, include_likelihood=True)
    mu_sample = gpr.predict(X_sample)

    sigma = sigma.reshape(-1, 1)

    # Needed for noise-based model,
    # otherwise use np.max(Y_sample).
    # See also section 2.4 in [1]
    mu_sample_opt = np.max(mu_sample)

    with np.errstate(divide='warn'):
        imp = mu - mu_sample_opt - xi
        Z = imp / sigma
        ei = imp * ss.norm.cdf(Z) + sigma * ss.norm.pdf(Z)
        ei[sigma == 0.0] = 0.0

    return ei


def propose_location(acquisition, X_sample, Y_sample, gpr, bounds, n_restarts=25):
    '''
    Proposes the next sampling point by optimizing the acquisition function.

    Args:
        acquisition: Acquisition function.
        X_sample: Sample locations (n x d).
        Y_sample: Sample values (n x 1).
        gpr: A GaussianProcessRegressor fitted to samples.

    Returns:
        Location of the acquisition function maximum.
    '''
    dim = X_sample.shape[1]
    min_val = 1
    min_x = None

    def min_obj(X):
        # Minimization objective is the negative acquisition function
        return -acquisition(X.reshape(-1, dim), X_sample, Y_sample, gpr)

    # Find the best optimum by starting from n_restart different random points.
    for x0 in np.random.uniform(bounds[:, 0], bounds[:, 1], size=(n_restarts, dim)):
        res = minimize(min_obj, x0=x0, bounds=bounds, method='L-BFGS-B')
        if res.fun < min_val:
            min_val = res.fun[0]
            min_x = res.x

    return min_x.reshape(-1, 1)

BOWOGPResult = namedtuple('BOWOGPResult', ('gp', 'Y_avg', 'Y_var','duration_in_seconds','mse'))
