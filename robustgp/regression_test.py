import numpy as np
from regression_runAll import RUN
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 22})

# based on code from https://nbviewer.jupyter.org/github/syrte/robustgp/blob/master/notebook/Example_Neal_Dataset.ipynb

total_runs = 10

n = 500

outputDir = "regression"

methods = ['TRADGP', 'ITGP','HUBGP', 'SEQGP']
n_methods = 4
lms_percentage = (1, 3, 10, 30)
for outlier_type_number in np.arange(9,11):
    t = {}
    mse = {}
    for m in methods:
        t[m] = None
        mse[m] = None
        for index,l in enumerate(lms_percentage):
            if index == 0:
                t[m]= {l:None}
                mse[m] = {l:None}
            else:
                t[m][l] = None
                mse[m][l] = None
            # t = {'TRADGP': {15: None, 80: None}, 'ITGP': {15: None, 80: None}, 'BOWOGP': {15: None, 80: None},
            #      'TLIKE': {15: None, 80: None}, 'HUBGP': {15: None, 80: None}, 'SEQGP': {15: None, 80: None}}
            # mse = {'TRADGP': {15: None, 80: None}, 'ITGP': {15: None, 80: None}, 'BOWOGP': {15: None, 80: None},
            #        'TLIKE': {15: None, 80: None}, 'HUBGP': {15: None, 80: None}, 'SEQGP': {15: None, 80: None}}
    for index, lmpercentage in enumerate(lms_percentage):
        print("_______LMS"+str(lmpercentage)+"__________")
        output = RUN(outlier_type_number,n,percentage_landmarks=lmpercentage,total_number_iterations=total_runs,type = outputDir) #type = 'regression')
        for indexmeth,m in enumerate(methods):
            t[m][lms_percentage[index]]= output[indexmeth]
            mse[m][lms_percentage[index]] = output[indexmeth + n_methods]
    for m in methods:
        plt.figure(figsize=(20, 15))
        plt.boxplot([mse[m][lms_percentage[0]], mse[m][lms_percentage[1]], mse[m][lms_percentage[2]], mse[m][lms_percentage[3]]], showmeans=True)
        plt.title('method ' + m)
        plt.xlabel([str(lms_percentage[0]), str(lms_percentage[1]), str(lms_percentage[2]), str(lms_percentage[3])])
        plt.ylabel("mean squared error")
        plt.savefig(outputDir+"/method" + m + "case" + str(outlier_type_number) + "mseVSlms.pdf")
        plt.figure(figsize=(20, 15))
        plt.boxplot([t[m][lms_percentage[0]], t[m][lms_percentage[1]], t[m][lms_percentage[2]], t[m][lms_percentage[3]]], showmeans=True)
        plt.xlabel([str(lms_percentage[0]), str(lms_percentage[1]), str(lms_percentage[2]), str(lms_percentage[3])])
        plt.title('method' + m)
        plt.ylabel("time")
        plt.savefig(outputDir+"/method" + m + "case" + str(outlier_type_number) + "tVSlms.pdf")
    plt.close('all')
