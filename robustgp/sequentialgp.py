
import GPy
import numpy as np
from matplotlib import pyplot as plt
from collections import namedtuple
import time

__all__ = ['SEQGP']


def SEQGP(X, Y, X_true, Y_true, ix_sub, type, CI = 30, optimize_kwargs={}, **gp_kwargs):
    """
    Robust Gaussian Process Regression Based on Iterative Trimming.

    Parameters
    ----------
    X: array shape (n, d)
    Y: array shape (n, 1)
    ix_sub: array with initial inlier indices
    CI: confidence interval for computing threshold, between 0 and 100

    Returns
    -------
    ITGPResult: named tuple object
        gp:
            GPy.core.GP object.
        ix_sub:
            Boolean index for trimming sample.
        niter:
            Total iterations performed, <= 1 + nsh + ncc + nrw.
        Y_avg, Y_var:
            Expectation and variance of input data points. None if return_predict=False.
        threshold:
            computed threshold
    """
    start_time = time.time()

    # check parameters
    if X.ndim == 1:
        X = np.atleast_2d(X).T
    if X_true.ndim == 1:
        X_true = np.atleast_2d(X).T
    if Y.ndim == 1:
        Y = np.atleast_2d(Y).T
    if len(X) != len(Y):
        raise ValueError("X should have the same length as Y")

    n, p = Y.shape
    if p != 1:
        raise ValueError("Y is expected in shape (n, 1).")

    gp_kwargs.setdefault('likelihood', GPy.likelihoods.Gaussian(variance=0.1))
    gp_kwargs.setdefault('kernel', GPy.kern.RBF(X.shape[1]))
    gp_kwargs.setdefault('name', 'SEQGP regression')

    # use copies so that input likelihood and kernel will not be changed
    likelihood_init = gp_kwargs['likelihood'].copy()
    kernel_init = gp_kwargs['kernel'].copy()

    # temp vars declaration
    d_mahal = None
    ix_sub_old = None
    n_inliers_old = 0
    n_inliers = len(ix_sub)

    # shrinking and concentrating
    niter = 0
    while n_inliers!=n_inliers_old:
        niter = niter+1
        n_inliers_old = n_inliers

        # train GP
        if type == 'regression':
            gp = GPy.models.GPRegression(X[ix_sub], Y[ix_sub], kernel_init, noise_var=0.1)
        else:
            gp = GPy.core.GP(X[ix_sub], Y[ix_sub], **gp_kwargs)
        gp.optimize(**optimize_kwargs)
        #plt.figure(figsize=(20, 15))
        Y_avg, Y_var = gp.predict(X, include_likelihood=True)
        d_mahal = (np.log(np.sqrt((Y - Y_avg)**2 / Y_var))).ravel()#((Y - Y_avg)**2 / Y_var).ravel()
        #plt.scatter(X, Y_avg,c='r')
        #plt.scatter(X[ix_sub], Y[ix_sub])
        #plt.show()

        #get threshold
        threshold = np.percentile(d_mahal[ix_sub],50+CI*0.5)

        niter_inner = 0
        n_inliers_inner = n_inliers
        n_inliers_inner_old = 0
        while n_inliers_inner_old!=n_inliers_inner:#n_inliers!=n_inliers_old:#ix_sub!=ix_sub_old:
            niter_inner = niter_inner+1
            ix_sub_old = ix_sub
            ix_sub = [index for index,value in enumerate(d_mahal) if ((((ix_sub_old.__contains__(index-1))
                                                                        or (ix_sub_old.__contains__(index+1))
                                                                           # or (ix_sub_old.__contains__(index+2))
                                                                            #or (ix_sub_old.__contains__(index+3))
                                                                                #or (ix_sub_old.__contains__(index+4))
                                                                                 #   or (ix_sub_old.__contains__(index+5))
                                                                                #        or (ix_sub_old.__contains__(index-2))
                                                                                 #           or (ix_sub_old.__contains__(index-3))
                                                                                  #              or (ix_sub_old.__contains__(index-4))
                                                                                   #                 or (ix_sub_old.__contains__(index-5))
                                                                                         ) and (value < threshold))
                                                                      or ix_sub_old.__contains__(index))]
            n_inliers_inner_old = len(ix_sub_old)
            n_inliers_inner = len(ix_sub)

        n_inliers = len(ix_sub)
    if type == 'regression':
        gp = GPy.models.GPRegression(X[ix_sub], Y[ix_sub], kernel_init, noise_var=0.1)
    else:
        gp = GPy.core.GP(X[ix_sub], Y[ix_sub], **gp_kwargs)
    gp.optimize(**optimize_kwargs)
    Y_avg, Y_var = gp.predict(X, include_likelihood=True)
    Y_true_avg, _ = gp.predict(X_true, include_likelihood=True)
    diff = np.subtract(Y_true, Y_true_avg)
    diffsq = np.power(diff, 2.0)
    mse_new = np.mean(diffsq)
    #mse_new = sum((Y_true - Y_avg) ** 2) / len(Y_true)
    duration_in_seconds = time.time() - start_time

    return SEQGPResult(gp, ix_sub, niter, Y_avg, Y_var, threshold,duration_in_seconds,mse_new)

SEQGPResult = namedtuple('SEQGPResult', ('gp', 'ix_sub', 'niter', 'Y_avg', 'Y_var', 'threshold','duration_in_seconds','mse'))
